/*
    Kurtis's Text to Speech plugin for Hifi Chat

    If you have any questions get in contact with me at http://keybase.io/theguywho 
    If they can't back with a hash you better dash!

    So the reason why i'm making this is becuase for a long while that i have been in high fidelity. 
    I have been sick and tired of people complaining about stupid tedious shit. Like all i fucking
    hear in here is complain, complain, complain. So out of anger i have decided to take a few fucking
    hours. Literally one google search to find it, and bam this is a thing now, how about you stop
    complaining about chat. Get off your ass and stop complaining to HiFi about shit that isnt even
    their fucking problem.

    This code is licenced under that "awe yeah do me daddy" licence (AYDMD) which is a fancy way of saying Creative Commons License No Rights Reserved (CC0)
	Feel free to evolve this further and make it into something fancy, make daddy proud ( ° ʖ °)

*/
(function() {
    // Global Object
    var self = {
        app: "TC_TTSChat",
        webApp: "TC_TTSChat_Web",
        version: 1,
        httpURL: Script.resolvePath("html/tts.html"),
        chatChannel: 'Chat',
        systemChannel: 'System',
        playing: true
    };
    var settings;
    var window;
    var chatter = [];
    // debug 
    function debug(message) {
        if (Settings.getValue(self.app+'-debug')) {
            print(self.app+":"+message);
        }
    }
    // On Closing
    Script.scriptEnding.connect(function() {
        try {
            window.close(); 
        } catch (e) {
            debug('No Web window to close.');
        }
    });
    function webEvent(event) {
        if (typeof(event) === 'string') {
            try {
                event = JSON.parse(event);
                if (event.app === self.app) {
                    switch (event.cmd) {
                        case 'init':
                            debug('TTS Ready!');
                            self.playing = false;
                            break;
                        case 'playEnd': 
                            self.playing = false;
                            pushTTS();
                            break;
                    }
                }
            } catch (e) {
                debug('error on webEvent'+e.message);
            }
        }
    }
    function save() {
        Settings.setValue(self.app, settings);
    }
    function load() {
        try {
            settings = Settings.getValue(self.app);
            if (typeof(settings) === 'string') {
                settings = {
                    version: 1,
                    amplitude: 100,
                    wordgap: 0,
                    pitch: 50,
                    speed: 175,
                    variant: 'none'
                };
            }
            // add versioning here
            return true;
        } catch (e) {
            return false;
        }
    }
    function pushTTS() {
        if (!self.playing && chatter.length > 0) {
            debug('hitting here');
            if (!Users.getIgnoreStatus(chatter[0].avatarID) && Users.getAvatarGain(chatter[0].avatarID) !== -100 && AvatarManager.getAvatar(chatter[0].avatarI).sessionDisplayName !== '') {
                var username = AvatarManager.getAvatar(chatter[0].avatarID).sessionDisplayName;
                var soundSettings = {
                    amplitude: Users.getAvatarGain(chatter[0].avatarID) + settings.amplitude,
                    wordgap: settings.wordgap,
                    pitch: settings.pitch,
                    speed: settings.speed,
                    variant: settings.variant
                };
                self.playing = true;
                debug('sending event:\n');
                window.emitScriptEvent(JSON.stringify({app: self.webApp, cmd: 'say', message: username+' said '+chatter[0].message, soundSettings: soundSettings}));
                chatter.splice(0,1);
            } else {
                // remove it and try again.
                chatter.splice(0,1);
                pushTTS();
            }
        }
    }
    function chatEvent(channel, message, senderID, local) {
        if (channel === self.chatChannel && senderID !== MyAvatar.sessionUUID) {
            // its a chat message
            message = JSON.parse(message);
            switch (message.type) {
                case 'TransmitChatMessage': 
                    debug('Chat Message:'+JSON.stringify(message)+'\n');
                    chatter.push({
                        avatarID: senderID,
                        message: message.message
                    });
                    pushTTS();
                    break;
            }
        } else if (channel === self.systemChannel && local) {
            // formatting {app: 'app name', message: 'message'}
        }
    }
    // Init stuff
    function start() {
        load();
        window = new OverlayWebWindow({ title: 'tts-client', source: 'about:blank', width: 0, height: 0, visible: false });
        window.webEventReceived.connect(webEvent);
        window.setURL(self.httpURL);
        Messages.subscribe(self.chatChannel);
        Messages.subscribe(self.systemChannel);
        Messages.messageReceived.connect(chatEvent);
    }
    // Starting Part
    start();
}());